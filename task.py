import pygame
import time

import os
import sys
import random
import time

from tkMessageBox import showwarning, showerror

from state import *


class EObject():

        ''' This class defines the objects in the experiment '''

        def __init__(self, name, **kw):
                self.name = name

                path = 'audio/' + name + '.wav'
                self.sound = pygame.mixer.Sound('audio/' + name + '.wav')
                self.sound_len = self.sound.get_length()

                if not os.path.exists(path):
                        showwarning(message = 'The sound file ' + name + '.wav does not exists.')
                        

        def play(self):
                self.sound.play()
                time.sleep(self.sound_len)

        def length(self):
                return self.sound_len





class Task():

        '''
        This class defines the general task in a JRD test. 
        We read the configuration file, and initialize the program based on it.
        '''
        INPUT_NORMAL            = 0
        INPUT_NOTE              = 1
        INPUT_OBJECTS           = 2
        INPUT_TRIALS            = 3

        def __init__(self, fname = None):

                self.randomize = False
                self.blocks = 1
                self.objects = {}
                self.trials_info = []
                self.sequence = []
                self.mode = 0

                # or we can do it in other ways
                for item in ['standing', 'facing', 'pointing']:
                        self.objects[item] = EObject(item)

                if (fname != None):
                        self.config(fname)



        def config(self, fname):
                fd = open(fname, 'r')

                state = Task.INPUT_NORMAL
                process = 0

                line = fd.readline()
                while line:
                        line = line.strip()

                        if len(line) == 0:
                                line = fd.readline()
                                continue


                        if cmp(line[:3], 'End') == 0:
                                state = Task.INPUT_NORMAL

                        elif cmp(line, 'Note') == 0:
                                state = Task.INPUT_NOTE

                        elif cmp(line, 'Objects') == 0:
                                state = Task.INPUT_OBJECTS
                                process = process + 1

                        elif cmp(line, 'Trials') == 0:
                                state = Task.INPUT_TRIALS
                                process = process + 2

                        elif state == Task.INPUT_NORMAL:
                                line = line.split()
                                if cmp(line[0], 'Randomize') == 0 and cmp(line[3].lower(), 'yes') == 0:
                                        self.randomize = True
                                elif cmp(line[0], 'Blocks') == 0:
                                        self.blocks = int(line[1])

                        elif state == Task.INPUT_OBJECTS:
                                line = line.replace(' ', '').split(',')
                                self.objects[line[0]] = EObject(line[0])


                        elif state == Task.INPUT_TRIALS:
                                line = line.replace(' ', '').split(',')
                                self.trials_info.append(line)
               
                        line = fd.readline()


                fd.close()

                #some thing is missing
                if process != 3:
                        showerror(message = 'The configuration file is not in a correct format')
                        self.gui.Cancel2()
                        return
                        
                self.trials_info.sort()
                self.trials = len(self.trials_info)



        def set_mode(self, mode):
                self.mode = mode


        def set_joy(self, joy):
                self.joy = joy


        def set_gui(self, gui):
                self.gui = gui



        def shuffle(self):
                # do the randomization if necessary
                if not self.randomize:
                        return

                cnt = 1
                self.indice = []
                for i in range(1, self.trials):
                        if cmp(self.trials_info[i-1][self.mode], self.trials_info[i][self.mode]) != 0:
                                self.indice.append(cnt)
                                cnt = 1
                        else:
                                cnt = cnt + 1

                self.indice.append(cnt)


                self.sequence = []

                for cnt in range(self.blocks):
                        rand1 = []
                        rand2 = []

                        y = range(len(self.indice))
                        random.shuffle(y)

                        for i, j in zip(self.indice, y):
                                start_num = random.randint(0, (self.trials - 1) / i)
                                x = [start_num + y * self.trials / i for y in range(i)]
                                random.shuffle(x)
                                rand1 = rand1 + x
                                rand2 = rand2 + [j] * i

                        tmp = zip([cnt] * self.trials, rand1, rand2, range(self.trials))
                        self.sequence = self.sequence + tmp

                self.sequence.sort()

                # cut out the randomization indice
                self.sequence = [item[3] for item in self.sequence]


        def start(self):
                self.shuffle()
                self.trials_cnt = 0
                self.blocks_cnt = 0
                self.state = State(self.blocks, self.trials, self.mode)
                text = ', '.join(self.trials_info[self.sequence[self.trials_cnt]])
                self.gui.task_text.append(text + '\n')
                self.joy.sti()


        def Next(self):

                self.joy.cli()
                self.joy.axis_off()

                self.state.next()
                while self.state.get() != STATE_IDLE:
                        self.state.next()

                self.indicator_update()
                self.joy.clear_events()
                self.joy.sti()


        def phase_standing(self, event):
                if self.state.get_next() == STATE_STANDING:
                        self.objects['standing'].play()
                        obj = self.trials_info[self.sequence[self.trials_cnt]][0]
                        self.objects[obj].play()
                        self.state.next()


        def phase_facing(self, event):
                if self.state.get_next() == STATE_FACING:
                        self.objects['facing'].play()
                        obj = self.trials_info[self.sequence[self.trials_cnt]][1]
                        self.objects[obj].play()
                        self.state.next()


        def phase_pointing(self, event):
                if self.state.get_next() == STATE_POINTING:

                        instruction = self.objects['pointing']
                        obj = self.objects[self.trials_info[self.sequence[self.trials_cnt]][2]]

                        instruction.play()
                        obj.play()

                        self.start_time = time.time()
                        self.joy.cli()
                        self.state.next()
                        self.joy.clear_events()
                        self.joy.axis_on()


        def onJoyMotion(self, event):
                if self.state.get() == STATE_POINTING:
                        self.end_time, angle = self.joy.get_detail()
                        rt = (self.end_time - self.start_time) * 1000

                        tail = self.trials_info[self.sequence[self.trials_cnt]]
                        error = angle - int(tail[3])

                        self.state.next()
                        self.joy.sti()

                        response = [str(self.blocks_cnt), str(self.trials_cnt), str(int(rt)), str(angle), str(error), str(abs(error)), ]
                        response = response + tail

                        self.gui.result_text.append(', '.join(response) + '\n')

                        self.indicator_update()



        # the process for ending of a trial
        def indicator_update(self):
                self.trials_cnt = self.trials_cnt + 1
                if (self.trials_cnt == self.trials):
                        self.blocks_cnt = self.blocks_cnt + 1
                        self.trials_cnt = 0
                        self.sequence = self.sequence[self.trials:]

                if self.state.get() == STATE_IDLE:
                        text = ', '.join(self.trials_info[self.sequence[self.trials_cnt]])
                        self.gui.task_text.append(text + '\n')
                else:
                        self.joy.cli()




if __name__ == '__main__':
        pygame.init()

        task = Task('config/actual ball.txt')
        task.set_mode(1)
        task.shuffle()

