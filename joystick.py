

import pygame
import math
import time
import sys

from tkMessageBox import showerror


FREQ = 20      # determines the frequency for checking the joystick events 

class joystick():

        def __init__(self, root):
                self.cli()
                self.axis_off()
                self.root = root
                self.timestamp = 0

                if (pygame.joystick.get_count() < 1):
                        showerror(message = 'Oh no! No joystick detected.')
                        sys.exit(1)

                self.joy0 = pygame.joystick.Joystick(0)
                self.joy0.init()
                print self.joy0.get_name()

                root.after(FREQ, self.find_events)

        def __distance(self, x, y):
                return math.sqrt(x * x + y * y)

        # calculating the direction of the stick 
        def direction(self, x, y):
                res = math.atan2(x, y) / math.pi * 180
                return int(180 - res)



        # stop receiving any events from joystick
        def cli(self):
                self.disable = True

        # start receiving events from joystick
        def sti(self):
                pygame.event.clear()
                self.disable = False 

        # start receiving axis events from joystick
        def axis_on(self):
                self.axis_disable = False

        # stop receving axis events from joystick
        def axis_off(self):
                self.axis_disable = True

        def reset(self):
                pass


        #Because of a little bug in PyGame, get_axis() alawys give extraneous output making things very uncomfortable. I changed the source code, and recombiled in a ubuntu system
        def find_events(self):

                if not self.disable:
                        events = pygame.event.get(pygame.JOYBUTTONDOWN)
                        for event in events:
                                name = '<<JoyButton-%d>>' % event.button
                                print name
                                self.root.event_generate(name)

                if not self.axis_disable:
                        axis_x = self.joy0.get_axis(0)
                        axis_y = self.joy0.get_axis(1)

                        if self.__distance(axis_x, axis_y) > 0.8:
                                self.timestamp = time.time()
                                self.angle = self.direction(axis_x, axis_y)
                                print self.angle
                                self.root.event_generate('<<JoyMotion>>', x = self.direction(axis_x, axis_y)) 
                                self.axis_off()

                pygame.event.clear()
                self.root.after(FREQ, self.find_events)

        def clear_events(self):
                pygame.event.clear()

        def get_detail(self):
                # return time and angle
                print self.timestamp, self.angle
                return (self.timestamp, self.angle)


        
