
STATE_IDLE              = 0
STATE_STANDING          = 1
STATE_FACING            = 2
STATE_POINTING          = 3
STATE_END               = 4

class State():
        def __init__(self, blocks, trials, mode):
                self.it = 0
                self.states = range(4)

                if mode != 0:
                        self.states.remove(1)
                        if mode != 1:
                                self.states.remove(1)

                self.states = self.states * trials * blocks
                self.states.append(STATE_END)


        def get(self):
                return self.states[self.it]

        def get_next(self):
                return self.states[self.it + 1]

        def next(self):
                self.it = self.it + 1



if __name__ == '__main__':
        state = State(2, 3, 1)
