from Tkinter import *
from tkFileDialog import *
from tkMessageBox import askokcancel

import os
import time
import pygame

from joystick import joystick
from task import Task


import platform
sys = platform.system()

# config the gui arguments
ButtonWidth = 18
RadioWidth = 18
EntryWidth = 13
if (sys == 'Windows'):
	ButtonWidth = 24
	EntryWidth = 14
	RaidoWidth = 16


Font = ('Arial', '11')

class MyRadio():
        
        def __init__(self, root, text, items):

                self.var = IntVar()
                self.var.set(1)

                frame = LabelFrame(root, text = text, font = Font)
                for ind, item in enumerate(items):
                        Radiobutton(frame, text = item, font = Font, variable = self.var, value = ind, 
                                        anchor = W, width = 18, command = self.output).pack()

                frame.pack(pady = 20)

        def get(self):
                return self.var.get()

        def output(self):
                pass



class SText():

        def __init__(self, root, width, height):
                self.frame = Frame(root)
                self.text = Text(self.frame, height = height, width = width)
                scroll = Scrollbar(self.frame)
                scroll.config(command = self.text.yview)
                self.text.configure(yscrollcommand = scroll.set)

                scroll.pack(side = RIGHT, fill = Y)
                self.text.pack(side = LEFT)
                self.frame.pack(padx = 5, pady = 5)

        def append(self, content):
                self.text.insert(END, content)


        def clear(self):
                self.text.delete(1.0, END)

        def get(self):
                return self.text.get(1.0, END)



class MyButton(Button):

        def __init__(self, root, text, command):
                Button.__init__(self, root, text = text, relief = RAISED, command = command, width = ButtonWidth)
                self.pack(pady = 5)



class MyEntry():

        def __init__(self, root, text):
                self.frame = Frame(root)
                self.label = Label(self.frame, text = text, font = ('Arial', '11'))
                self.entry = Entry(self.frame, font = ('Arial', '11'), width = EntryWidth, relief = SUNKEN)

                self.label.pack(side = LEFT)
                self.entry.pack(side = LEFT)
                self.frame.pack(pady = 5)


        def get(self):
                return self.entry.get()



class GUI(Frame):

        def __init__(self, task):
                Frame.__init__(self)
                self.pack(side = TOP, expand = YES, fill = BOTH)
                self.master.title('JRD')


                frame_left = Frame(self)
                frame_right = Frame(self)

                frame_left.pack(side = LEFT, padx = 10)
                frame_right.pack(side = RIGHT, padx = 10)

                self.task_text = SText(frame_left, height = 15, width = 105) 
                self.result_text = SText(frame_left, height = 25, width = 105)

                self.open_button = MyButton(frame_right, text = 'OPEN', command = self.Browse)
                self.start_button = MyButton(frame_right, text = 'START', command = self.Start)
                self.cancel_button = MyButton(frame_right, text = 'CANCEL', command = self.Cancel)
                self.next_button = MyButton(frame_right, text = 'NEXT', command = self.Next)
                self.save_button = MyButton(frame_right, text = 'SAVE', command = self.Save)

                self.start_button.config(state = DISABLED)
                self.save_button.config(state = DISABLED)
                self.next_button.config(state = DISABLED)
                self.cancel_button.config(state = DISABLED)

                self.lb1 = MyEntry(frame_right, text = 'Sub No.: ')
                self.lb2 = MyEntry(frame_right, text = 'Sub Sex: ')

                self.radio = MyRadio(frame_right, 'MODE', ['JRD STD.', 'FACING&POINTING', 'POINTING'])


                #bind the JoyEvent with STANDING INSTRUCTION
                self.bind('<<JoyButton-1>>', task.phase_standing)

                #bind the JoyEvent with FACING INSTRUCTION
                self.bind('<<JoyButton-2>>', task.phase_facing)

                #bind the JoyEvent with POINTING INSTRUCTION
                self.bind('<<JoyButton-3>>', task.phase_pointing)

                self.bind('<<JoyMotion>>', task.onJoyMotion)


        def Start(self):
                self.open_button.config(state = DISABLED)
                self.start_button.config(state = DISABLED)

                self.save_button.config(state = NORMAL)
                self.next_button.config(state = NORMAL)
                self.cancel_button.config(state = NORMAL)

                task.config(self.config_file)
                task.set_mode(self.radio.get())

                task.start()

        def Next(self):
                task.Next()

        def Cancel(self):

                if askokcancel(message = 'Want to redo the experiments?'):
                        self.open_button.config(state = NORMAL)
                        self.start_button.config(state = DISABLED)
                        self.save_button.config(state = DISABLED)
                        self.next_button.config(state = DISABLED)
                        self.cancel_button.config(state = DISABLED)

                        self.task_text.clear()
                        self.result_text.clear()

        def Cancel2(self):
                self.open_button.config(state = NORMAL)
                self.start_button.config(state = DISABLED)
                self.save_button.config(state = DISABLED)
                self.next_button.config(state = DISABLED)
                self.cancel_button.config(state = DISABLED)

                self.task_text.clear()
                self.result_text.clear()


        def Browse(self):
                self.config_file = askopenfilename()

                if not self.config_file:
                        return
                
                self.start_button.config(state = NORMAL)


        def Save(self):
                save_file = asksaveasfilename()

                if not save_file:
                        return

                fd = open(save_file, 'w')

                fd.write('Subject\nSubject Number: ' + self.lb1.get() + '\n')
                fd.write('Subject Sex: ' + self.lb2.get() + '\nEnd Subject\n\n')

                fd.write('Experiment Date Time: ' + time.ctime() + '\n\n')

                fd.write('Response\n')
                fd.write(self.result_text.get())
                fd.write('End Response\n')

                fd.close()
                        


# Main
if __name__ == '__main__':
        pygame.init()

        root = Tk()
        root.resizable(False, False)
        task = Task()
        gui = GUI(task)
        joy = joystick(gui)
        task.set_joy(joy)
        task.set_gui(gui)

        root.mainloop()

